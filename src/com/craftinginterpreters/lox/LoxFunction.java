package com.craftinginterpreters.lox;

import java.util.List;

class LoxFunction implements LoxCallable {

    Stmt.Function declaration;
    Environment closure;
    boolean isInitializer;

    LoxFunction(Stmt.Function declaration, Environment closure,
            boolean isInitializer) {
        this.declaration = declaration;
        this.closure = closure;
        this.isInitializer = isInitializer;
    }

    @Override
    public Object call(Interpreter interpreter, List<Object> arguments) {
        Environment environment = new Environment(closure);

        for (int i = 0; i < declaration.params.size(); i++) {
            environment.define(declaration.params.get(i).lexeme,
                    arguments.get(i));
        }
        try {
            interpreter.executeBlock(declaration.body, environment);
        } catch (Return retValue) { // 偵測到 Return 語句
            if (isInitializer)
                return closure.getAt(0, "this"); // return this
            return retValue.value;
        }
        // 未偵測到 Return 語句
        if (isInitializer)
            return closure.getAt(0, "this"); // return this
        return null;
    }

    LoxFunction bind(LoxInstance instance) {
        Environment environment = new Environment(closure);
        environment.define("this", instance);
        return new LoxFunction(declaration, environment, isInitializer);
    }

    @Override
    public int arity() {
        return declaration.params.size();
    }
}
