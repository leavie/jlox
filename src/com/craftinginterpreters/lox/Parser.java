/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.craftinginterpreters.lox;

import static com.craftinginterpreters.lox.TokenType.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author leavie
 */
public class Parser {

    private static class ParseError extends RuntimeException {

    }

    private List<Token> tokens;
    private int current = 0;

    public Parser(List<Token> tokens) {
        this.tokens = tokens;
    }

    List<Stmt> parse() {
        try {
            List<Stmt> statements = new ArrayList<>();
            while (!isAtEnd()) {
                statements.add(declaration());
            }

            return statements;
        } catch (ParseError error) {
            return null;
        }
    }

    private Token advance() { // current++
        if (!isAtEnd()) {
            current++;
        }
        return previous();
    }

    private Token peek() {
        return tokens.get(current);
    }

    private Token previous() {
        return tokens.get(current - 1);
    }

    private ParseError error(Token token, String message) {
        Lox.error(token, message);
        return new ParseError();
    }

    private void synchronize() {
        advance();

        while (!isAtEnd()) {
            if (previous().type == SEMICOLON) {
                return;
            }

            switch (peek().type) {
                case CLASS:
                case FUN:
                case VAR:
                case FOR:
                case IF:
                case WHILE:
                case PRINT:
                case RETURN:
                    return;
            }

            advance();
        }
    }

    private boolean match(TokenType... types) { // current++
        for (TokenType type : types) {
            if (check(type)) {
                advance();
                return true;
            }
        }
        return false;
    }

    private Token consume(TokenType type, String message) {
        if (check(type)) {
            return advance();
        }
        throw error(peek(), message);
    }

    private boolean check(TokenType type) {
        if (isAtEnd()) {
            return false;
        }
        return peek().type == type;
    }

    private boolean isAtEnd() {
        return peek().type == EOF;
    }

    private Stmt declaration() {
        try {
            if (match(CLASS)) {
                return classDeclaration();
            }
            if (match(FUN)) {
                return funDeclaration("function");
            }
            if (match(VAR)) {
                return varDeclaration();
            }
            return statement();

        } catch (ParseError error) {
            synchronize();
            return null;
        }
    }

    private Stmt classDeclaration() { // classDecl -> "class" IDENTIFIER ("<" IDENTIFIER)? "{" function* "}";
        Token name = consume(IDENTIFIER, "Expect class name.");

        Expr.Variable superclass = null;
        if (match(LESS)) {
            Token superclassname;
            superclassname = consume(IDENTIFIER, "Expect superclass name after '<'.");
            superclass = new Expr.Variable(superclassname);
        }

        consume(LEFT_BRACE, "Expect '{' after class body.");
        List<Stmt.Function> methods = new ArrayList<>();
        while (!check(RIGHT_BRACE) && !isAtEnd()) {
            methods.add(funDeclaration("method"));
        }
        consume(RIGHT_BRACE, "Expect '}' after class body.");
        return new Stmt.Class(name, superclass, methods);
    }

    private Stmt.Function funDeclaration(String kind) {
        List<Token> parameters = new ArrayList<>();

        Token name = consume(IDENTIFIER, "Expect " + kind + " name.");
        consume(LEFT_PAREN, "Expect '(' after " + kind + " name.");

        if (!check(RIGHT_PAREN)) {
            do {
                if (parameters.size() >= 255) {
                    error(peek(), "Can't have more than 255 parameters.");
                }

                parameters.add(
                        consume(IDENTIFIER, "Expect parameter name."));
            } while (match(COMMA));
        }

        consume(RIGHT_PAREN, "Expect ')' after parameters.");

        consume(LEFT_BRACE, "Expect '{' before " + kind + " body."); //如果{没有找到，因为我们知道它在函数声明的上下文中。
        List<Stmt> body = block();

        return new Stmt.Function(name, parameters, body);
    }

    private Stmt varDeclaration() {
        Token name = consume(IDENTIFIER, "Expect variable name.");

        Expr initializer = null;
        if (match(EQUAL)) {
            initializer = expression();
        }

        consume(SEMICOLON, "Expect ';' after variable declaration.");
        return new Stmt.Var(name, initializer);
    }

    private Stmt statement() {
        if (match(PRINT)) {
            return printStatement();
        }

        if (match(LEFT_BRACE)) {
            return new Stmt.Block(block());
        }

        if (match(IF)) {
            return ifStatement();
        }

        if (match(WHILE)) {
            return whileStatement();
        }

        if (match(FOR)) {
            return forStatement();
        }

        if (match(RETURN)) {
            return retStatment();
        }

        return expressionStatement();
    }

    private Stmt retStatment() {
        Token keyword = previous();
        Expr value = null;

        if (!check(SEMICOLON)) {
            value = expression();
        }

        consume(SEMICOLON, "Expect ';' after return value.");
        return new Stmt.Return(keyword, value);
    }

    private List<Stmt> block() {
        List<Stmt> statments = new ArrayList<>();
        while (!isAtEnd() && !check(RIGHT_BRACE)) {
            statments.add(declaration());
        }
        consume(RIGHT_BRACE, "Expect '}' after block.");
        return statments;
    }

    private Stmt ifStatement() {
        consume(LEFT_PAREN, "Expect '(' after if.");
        Expr condition = expression();
        consume(RIGHT_PAREN, "Expect ') after if condition.");

        Stmt thenBranch = statement();
        Stmt elseBranch = null;

        if (match(ELSE)) {
            elseBranch = statement();
        }
        return new Stmt.If(condition, thenBranch, elseBranch);
    }

    private Stmt whileStatement() {
        consume(LEFT_PAREN, "Expect '(' after while.");
        Expr condition = expression();
        consume(RIGHT_PAREN, "Expect ') after while condition.");

        Stmt body = statement();
        return new Stmt.While(condition, body);
    }

    private Stmt forStatement() {
        Stmt initializer = null;
        Expr condition = null;
        Stmt increment = null;

        consume(LEFT_PAREN, "Expect '(' after for.");
        if (match(VAR)) {
            initializer = varDeclaration();
        } else if (!check(SEMICOLON)) {
            initializer = expressionStatement();
        } else {
            consume(SEMICOLON, "Expect ';' loop initializer.");
        }

        if (!check(SEMICOLON)) {
            condition = expression();
        }
        consume(SEMICOLON, "Expect ';' loop condition.");

        if (!check(RIGHT_PAREN)) {
            increment = new Stmt.Expression(expression());
        }
        consume(RIGHT_PAREN, "Expect ')' ");

        Stmt body = statement();

        if (increment != null) {
            body = new Stmt.Block(Arrays.asList(body, increment));
        }

        if (condition == null) {
            condition = new Expr.Literal(true);
        }

        Stmt whileStmt = new Stmt.While(condition, body);

        if (initializer != null) {
            return new Stmt.Block(
                    Arrays.asList(initializer, whileStmt));
        }

        return whileStmt;
    }

    private Stmt printStatement() {
        Expr value = expression();
        consume(SEMICOLON, "Expect ';' after value.");
        return new Stmt.Print(value);
    }

    private Stmt expressionStatement() {
        Expr expr = expression();
        consume(SEMICOLON, "Expect ';' after expression.");
        return new Stmt.Expression(expr);
    }

    private Expr expression() {
        return assignment();
    }

    private Expr assignment() {
        Expr expr = or();

        if (match(EQUAL)) {
            Token equals = previous();
            Expr value = assignment();

            if (expr instanceof Expr.Variable) {
                Token name = ((Expr.Variable) expr).name;
                return new Expr.Assign(name, value);
            } else if (expr instanceof Expr.Get) {
                Expr.Get get = (Expr.Get) expr;
                return new Expr.Set(get.object, get.name, value);
            }

            error(equals, "Invalid assignment target.");
        }
        return expr;
    }

    private Expr or() {
        Expr expr = and();

        while (!isAtEnd() && match(OR)) {
            expr = new Expr.Logical(expr, previous(), and());
        }

        return expr;
    }

    private Expr and() {
        Expr expr = equality();

        while (!isAtEnd() && match(AND)) {
            expr = new Expr.Logical(expr, previous(), equality());
        }

        return expr;

    }

    private Expr equality() {
        Expr expr = comparasion();

        while (match(BANG_EQUAL, EQUAL_EQUAL)) {
            expr = new Expr.Binary(expr, previous(), comparasion());
        }
        return expr;
    }

    private Expr comparasion() {
        Expr expr = term();

        while (match(GREATER, GREATER_EQUAL, LESS, LESS_EQUAL)) {
            expr = new Expr.Binary(expr, previous(), term());
        }
        return expr;
    }

    private Expr term() {
        Expr expr = factor();

        while (match(PLUS, MINUS)) {
            Token operator = previous();
            Expr right = factor();
            expr = new Expr.Binary(expr, operator, right);
        }
        return expr;
    }

    private Expr factor() {
        Expr expr = unary();

        while (match(STAR, SLASH)) {
            expr = new Expr.Binary(expr, previous(), unary());
        }
        return expr;
    }

    private Expr unary() { // recursion
        if (match(MINUS, BANG)) {
            return new Expr.Unary(previous(), unary());
        }
        return call();
    }

    private Expr call() {
        Expr expr = primary();

        while (true) {
            if (match(LEFT_PAREN)) {
                expr = finishCall(expr);
            } else if (match(DOT)) {
                expr = new Expr.Get(expr, consume(IDENTIFIER,
                        "Expect property name after '.'."));
            } else {
                break;
            }
        }
        return expr;
    }

    private Expr finishCall(Expr callee) {
        List<Expr> arguments = new ArrayList<>();

        if (!check(RIGHT_PAREN)) {
            do {
                if (arguments.size() >= 255) {
                    error(peek(), "Can't have more than 255 arguments.");
                }
                arguments.add(expression());
            } while (match(COMMA));
        }

        Token paren = consume(RIGHT_PAREN,
                "Exppect ')' after arguments.");
        return new Expr.Call(callee, paren, arguments);

    }

    private Expr primary() {
        if (match(TRUE)) {
            return new Expr.Literal(true);
        }
        if (match(FALSE)) {
            return new Expr.Literal(false);
        }
        if (match(NIL)) {
            return new Expr.Literal(null);
        }

        if (match(THIS)) {
            return new Expr.This(previous());
        }

        if (match(NUMBER, STRING)) {
            return new Expr.Literal(previous().literal);
        }

        if (match(SUPER)) {
            Token keyword = previous();
            consume(DOT, "Expect '.' after 'super'.");
            consume(IDENTIFIER, "Expect superclass method name.");
            return new Expr.Super(keyword, previous());
        }

        if (match(IDENTIFIER)) {
            return new Expr.Variable(previous());
        }

        if (match(LEFT_PAREN)) { // ((1+2)+(3+4))

            Expr expr = new Expr.Grouping(expression());
            consume(RIGHT_PAREN, "Expect ')' after expresson.");
            return expr;

        }

        // unrecognized token
        throw error(peek(), "Expect expression.");
    }
}
/**
 * 3 + 0 == 4 != 5 > 7 - 8 != (( 13 + 2) - 1)
 *
 */
/**
 * expression → assignment ;
 * assignment -> (call ".")? IDENTIFIER "=" assignment | logical_or ;
 * logical_or -> logical_and ( "||" logical_and )* ;
 * logical_and -> equality ( "&&" equality )* ;
 * equality → comparison ( ( "!=" | "==" ) comparison )*;
 * comparison → term ( ( ">" | ">=" | "<" | "<=" ) term )* ;
 * term → factor (("-" | "+" ) factor )* ;
 * factor → unary ( ( "/" | "*" ) unary )* ;
 * unary → ("!" | "-" ) unary | call ;
 * call -> primary ( "(" arguments? ")" | "." IDENTIFIER )* ;
 * primary → "true" | "false" |"nil"| "this"
 * | NUMBER | STRING | IDENTIFIER | "(" expression ")" ;
 * | "super" "." IDENTIFIER
 */
