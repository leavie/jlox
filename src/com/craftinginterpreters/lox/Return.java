/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.craftinginterpreters.lox;

/**
 *
 * @author leavie
 */
public class Return extends RuntimeException {

    Object value;

    Return(Object value) {
        super(null, null, false, false);
        this.value = value;
    }
}
