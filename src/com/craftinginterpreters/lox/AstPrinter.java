/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.craftinginterpreters.lox;

/**
 *
 * @author leavie
 */
public class AstPrinter implements Expr.Visitor<String> {

    @Override
    public String visitCallExpr(Expr.Call expr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visitLogicalExpr(Expr.Logical expr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visitAssignExpr(Expr.Assign expr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visitVariableExpr(Expr.Variable variable) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    String print(Expr expression) {
        return expression.accept(this);
    }

    @Override
    public String visitUnaryExpr(Expr.Unary unary) {
        return "(" + unary.operator.lexeme + " " + unary.right.accept(this) + ")";
    }

    @Override
    public String visitLiteralExpr(Expr.Literal literal) {
        return literal.value == null ? "nil" : literal.value.toString();
    }

    @Override
    public String visitGroupingExpr(Expr.Grouping grouping) {
        return "(group " + grouping.expression.accept(this) + ")";
    }

    @Override
    public String visitBinaryExpr(Expr.Binary binary) {
        return "(" + binary.operator.lexeme + " " + binary.left.accept(this)
                + " " + binary.right.accept(this) + ")";
    }

    public static void main(String[] args) {
        Expr expression = new Expr.Binary(
                new Expr.Unary(
                        new Token(TokenType.MINUS, "-", null, 1),
                        new Expr.Literal(123)),
                new Token(TokenType.STAR, "*", null, 1),
                new Expr.Grouping(new Expr.Literal(45.67))
        );
        System.out.println(new AstPrinter().print(expression));
    }

    @Override
    public String visitGetExpr(Expr.Get expr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visitSetExpr(Expr.Set expr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String visitThisExpr(Expr.This expr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
