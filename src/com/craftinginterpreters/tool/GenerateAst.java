/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.craftinginterpreters.tool;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author leavie
 */
public class GenerateAst {

    private static void defineAst(String outputDir, String baseName,
            List<String> types) throws Exception {
        String path = outputDir + "/" + baseName + ".java";
        PrintWriter writer = new PrintWriter(path, "utf-8");

        writer.println("package com.craftinginterpreters.lox;");
        writer.println();
        writer.println("import java.util.List;");
        writer.println();
        writer.println("abstract class " + baseName + " {");

        defineVistor(writer, baseName, types);
        writer.println();
        writer.println("    abstract <R> R accept(Visitor<R> vistor);");

        for (String type : types) {
            String className = type.split(":")[0].trim();
            String fields = type.split(":")[1].trim();
            defineType(writer, className, baseName, fields);
        }
        writer.println("}");

        writer.close();

    }

    private static void defineType(PrintWriter writer, String className,
            String baseName, String fields) {

        writer.println();
        writer.println(
                "    static class " + className + " extends " + baseName + " {");
        writer.println();
        writer.println("        " + className + "(" + fields + ") {");
        for (String field : fields.split(",")) {
            String name = field.trim().split(" ")[1].trim();
            writer.println("            this." + name + " = " + name + ";");
        }
        writer.println("        }");

        writer.println();
        for (String field : fields.split(",")) {
            writer.println("        final " + field.trim() + ";");
        }

        writer.println();
        writer.println("        @Override");
        writer.println("        <R> R accept(Visitor<R> visitor) {");
        writer.println(
                "            return visitor.visit" + className + baseName + "(this);");
        writer.println("        }");
        writer.println("    }");

    }

    private static void defineVistor(PrintWriter writer, String baseName,
            List<String> types) {
        writer.println();
        writer.println(
                "    interface Visitor<R> {");
        for (String type : types) {
            String className = type.split(":")[0].trim();
            writer.println();
            writer.println(
                    "        R visit" + className + baseName + "(" + className + " "
                    + baseName.toLowerCase() + ");");
        }
        writer.println("    }");
    }

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Usage: generate_ast <output directory>");
            System.exit(64);
        }
        String outputDir = args[0];
        defineAst(outputDir, "Expr", Arrays.asList(
                "Assign   : Token name, Expr value",
                "Binary   : Expr left, Token operator, Expr right",
                "Logical   : Expr left, Token operator, Expr right",
                "Grouping : Expr expression",
                "Literal  : Object value",
                "Unary    : Token operator, Expr right",
                "Variable : Token name",
                "Call     : Expr callee, Token paren, List<Expr> arguments",
                "Get      : Expr object, Token name",
                "Set      : Expr object, Token name, Expr value",
                "This     : Token keyword",
                "Super    : Token keyword, Token method"
        ));
        defineAst(outputDir, "Stmt", Arrays.asList(
                "Expression : Expr expression",
                "If         : Expr condition, Stmt thenBranch, Stmt elseBranch",
                "While      : Expr condition, Stmt body",
                // "For        : Stmt initializer, Expr condition, Expr increment, Stmt body",
                // Desugaring from front end, so don't define this syntax node.
                "Print      : Expr expression",
                "Return     : Token keyword, Expr value",
                "Var        : Token name, Expr initializer",
                "Function: Token name, List<Token> params, List<Stmt> body",
                "Class: Token name, Expr.Variable superclass, List<Stmt.Function> methods",
                "Block      : List<Stmt> statements"
        ));
    }
}
